$(function(){
  var language = window.localStorage.getItem('language');
  if (language) {
    setLanguage(language)
  } else {
    setLanguage('en')
  }
  initFormHelp();
  initCountDown();
  $('.js-toggle-form').click(function(){
    $('#transport-help').toggleClass('hidden');

    $('.js-help').toggleClass('hidden');
    $('.js-nohelp').toggleClass('hidden');
  })

  $('.js-scroll-to-program').click(function(e) {
    e.preventDefault();
    $([document.documentElement, document.body]).animate({
        scrollTop: $('#program').offset().top - 30
    }, 2000);
});

});


$('.select-lang').click(function(ev){
  var setLang = $(this).data('lang');
  setLanguage(setLang)
})
//" data-lang="fr"

function setLanguage(lang) {
  $('body').removeClass('lang-fr lang-sv lang-en');
  $('body').addClass('lang-' + lang);
  window.localStorage.setItem('language', lang);
}


// Register the service worker
// if ('serviceWorker' in navigator) {
// 	navigator.serviceWorker.register('./js/service-worker.js').then(function(registration) {
//     // Registration was successful
//     console.log('ServiceWorker registration successful with scope: ', registration.scope);
// }).catch(function(err) {
//     // registration failed :(
//     	console.log('ServiceWorker registration failed: ', err);
//     });
// }


function initFormHelp() {

  var $form = $('form#transport-help'),
  url = 'https://script.google.com/macros/s/AKfycbwW_p8cAdwGLyLyUollCrjXmZTGuscUG8QC-zY6GpLiXZ7Jf8k/exec'

  $form.validate({
    errorLabelContainer: '.error-class',
    messages: {
      names: "We need your name(s).",
      staying: "We need to know where your staying.",
      accommodation: "We need to know the type of accommodation.",
      pp: "We need to know how many people.",
      names_brunch: "We need your name(s).",
      attendance_brunch: "We need to know how many people.",
      people_brunch: "We need to know if your coming or not"
    },
    errorElement: "div",
    wrapper: "div",
    submitHandler: function (form) {
      if ($(form).valid()) {
        $('.js-submit-btn').addClass('btn-loading').html('Loading...')
        var jqxhr = $.ajax({
          url: url,
          method: "GET",
          dataType: "json",
          data: $form.serialize(),
          error: function (err) {
            console.log('something went wrong', err)
          },
          complete: function () {
            $('#transport-help').hide();
            $('.js-toggle-form').hide();
            $('.form-success').removeClass('hidden');
          }
        })

      } else {
        alert("Something went wrong, please refresh and start again")
      }


    }
  });


}

function initCountDown() {

    var then = new Date(2019, 06, 07), // month is zero based
        now = new Date;                // no arguments -> current date
    var rightNow = 0;
    // 24 hours, 60 minutes, 60 seconds, 1000 milliseconds
    var daysleft = Math.round((then - now) / (1000 * 60 * 60 * 24)) + 1; // round the amount of days
    // result: 712
    //$('.countdown').text(daysleft);
    //if (daysleft < 0) daysleft = 0;
    console.log(daysleft);
    // while (daysleft > rightNow) {
    //   $('.js-countdown').html(rightNow)
    //   rightNow++;
    // }
    var timer = setInterval(updatenumber, 5);
    function updatenumber() {
      $('.js-countdown').html(rightNow)
      rightNow++;
      if (rightNow >= daysleft) {
        clearInterval(timer);
      }
    }
}
