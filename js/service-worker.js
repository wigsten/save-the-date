var cacheVersion = 4;
var currentCache = {
  offline: 'offline-cache' + cacheVersion
};
const offlineUrl = 'offline-page.html';

console.log('registering offline storage', currentCache)

var listOfStuffs = [
  '../index.html'
]

this.addEventListener('install', event => {
  event.waitUntil(
    caches.open(currentCache.offline).then(function(cache) {
      return cache.addAll(listOfStuffs);
    })
  );
});